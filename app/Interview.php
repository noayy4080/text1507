<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class Interview extends Model
{
    protected $fillable = ['date','summary','candidate_id','user_id'];

    public function candidate(){
        return $this->hasOne('App\Candidate');
    }

   // public function user(){
     //   return $this->belongsTo('App\User');
    //}

    public function owner(){
        return $this->belongsTo('App\User','user_id');
    }

    public static function myInter(){
        $my = DB::table('interviews')->where('user_id',Auth::user())->pluck('id');
        return self::find($my)->all(); 
    }

}
