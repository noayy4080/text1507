<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InterviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


public function run()
{
    DB::table('interviews')->insert([
        [
            'date' => '2000-01-01',
            'summary' => Str::random(30),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],
        [
            'date' => '2000-01-01',
            'summary' => Str::random(30),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],                      
        ]);            
}
}
