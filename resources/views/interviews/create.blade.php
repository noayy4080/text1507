@extends('layouts.app')

@section('title', 'Create Interview')

@section('content')
        <h1>Create Interview</h1>
        <form method = "post" action = "{{action('InterviewsController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "date">Interview date</label>
            <input type = "date" class="form-control" name = "date">
        </div>  

        <!--add candidate-->
        <div class="form-group">
                    <label for="candidate_id" class="col-md-4">Move to candidate</label>
                        <select class="form-control" name="candidate_id">                                                                         
                          @foreach ($candidates as $candidate)
                          <option value="{{ $candidate->id }}"> 
                              {{ $candidate->name }} 
                          </option>
                          @endforeach    
                        </select>
        </div>

        <!--add user-->
        <div class="form-group">

        
                    <label for="candidate_id" class="col-md-4">Move to User</label>

                        <select class="form-control" name="user_id">                                                           
                          @foreach ($users as $user)
                          <option value="{{ $user->id }}"> 
                              {{ $user->name }} 
                          </option>
                          @endforeach    
                        </select>
        </div>

        <div class="form-group">
            <label for = "summary">Interview summary</label>
            <input type = "text" class="form-control" name = "summary">
        </div> 

        <input name="id" type="hidden" value = {{$candidate->id, $user->id}} >
                    <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    create interview
                                </button>
                            </div>
                    </div>                     
        </form>    
@endsection
