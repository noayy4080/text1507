@extends('layouts.app')

@section('title', 'Interview')

@section('content')


<div><a href =  "{{url('/interviews/create')}}"> Create Interviews </a></div>
<h1>List of Interviews</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>date</th><th>candidate</th><th>user</th><th>summary</th><th>Created</th><th>Updated</th>
    </tr>
    <!-- the table data -->

    @foreach($interviews as $interview)

        <tr>
            <td>{{$interview->id}}</td>
            <td>{{$interview->date}}</td>

            <td>
            {{$interview->candidate_id}}  
            </td>

            <td>
            {{$interview->user_id}}  
            </td>

            <td>{{$interview->summary}}</td>      
            <td>{{$interview->created_at}}</td>                                        
            <td>{{$interview->updated_at}}</td>                                        
                                  
        </tr>

    @endforeach
</table>
@endsection

